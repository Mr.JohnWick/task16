


import java.io.*;
import java.util.ArrayList;

public class Task16 {
    public static void main(String[] args) {

        if(args.length > 1) {
            String filename = args[0];
            String currentLine = "";
            String searchWord = args[1];
            ArrayList<String>resultOfSearch = new ArrayList<String>();
            int lineNumber = 0;
            int numberOfFindings = 0;
            long fileSize = 0;
            String[] words;

            File file = new File(filename);
            try{

                BufferedReader reader = new BufferedReader(new FileReader(file));
                fileSize = file.length();
                while((currentLine = reader.readLine()) != null){
                    words = currentLine.split(" ");
                    for(String word: words) {
                        if (word.toLowerCase().contains(searchWord.toLowerCase())) {
                            resultOfSearch.add(word);
                            numberOfFindings++;
                        }
                    }
                    lineNumber++;
                }
                System.out.println("File size: "+fileSize+" Bytes");
                System.out.println("Number of lines "+lineNumber+" lines");
                System.out.print("Found these words: ");
                resultOfSearch.forEach(System.out::println);
                System.out.println("Word(s) found "+" number of times: "+numberOfFindings);
            }catch (FileNotFoundException e){
                System.out.println("File not found threw "+e);
            }catch(IOException e) {
                System.out.println("Threw IOException " + e);
            }
        }else{
            System.out.println("Please provide a filename and a search word");
        }
    }
}
